import { StyleSheet, Text, View, Image, TextInput, SafeAreaView, FlatList, listExpression, renderItem, Tab } from 'react-native';
import React, { useState, useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

export default function App() {
  const [character, setCharacter] = useState(null);
  const [quote, setQuote] = useState(null);
  const [icon, setIcon] = useState(null);
  const [input, setInput] = useState(null);
  const [listQuote, setlistQuote] = useState(null);

  const Tab = createBottomTabNavigator();

  const getChar = () => {
    return fetch('https://thesimpsonsquoteapi.glitch.me/quotes')
        .then((response) => response.json())
        .then((json) => {
          setCharacter('Nom: ' + json[0].character)
          setQuote('Citation: ' + json[0].quote)
          setIcon(json[0].image)
          return json.name
        })
        .catch((error) => {
          console.error(error);
        });
  };

  const getNomChar = () => {
    return fetch('https://thesimpsonsquoteapi.glitch.me/quotes?character=' + input)
        .then((response) => response.json())
        .then((json) => {
          setCharacter('Nom: ' + json[0].character)
          setQuote('Citation: ' + json[0].quote)
          setIcon(json[0].image)
          console.log(input)
          console.log(json[0].character)
          return json.name
        })
        .catch((error) => {
          console.error(error);
        });
  }

  function Random() {
    return (
      <View style={styles.carte}>
        <View style={styles.carte2}>
          <Image
            style={styles.tinyLogo}
            source={{uri: icon}}
          />
          <View style={styles.textcarte}>
            <Text style={styles.text2}>{character}</Text>
            <Text style={styles.text2}>{quote}</Text>
          </View>
        </View>
        <View style={styles.samelesbrise}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              getChar();
            }}
          >
            <Text>Reload</Text>
          </TouchableOpacity>
        </View>
      </View>

    );
  }

  useEffect(() => {
    (async () => {
      getChar();
    })();
  }, []);


  function Recherche() {
    return (
      <>
      <View style={styles.recherche}>
          <TextInput style={styles.input} placeholder="Entrez un nom" onChangeText={item => setInput(item)}/>
          <TouchableOpacity
              style={styles.button2}
              onPress={() => {
                getNomChar();
              }}
            >
              <Text>Chercher</Text>
          </TouchableOpacity>
      </View>

      <View style={styles.carte}>
        <View style={styles.carte2}>
          <Image
            style={styles.tinyLogo}
            source={{uri: icon}}
          />
          <View style={styles.textcarte}>
            <Text style={styles.text2}>{character}</Text>
            <Text style={styles.text2}>{quote}</Text>
          </View>
        </View>
    </View>
    </>
    );
  }

  return (
    <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen name="Random" component={Random} />
          <Tab.Screen name="Recherche" component={Recherche} />
        </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  separator: {
    paddingBottom: 20
  },
  input: {
    borderWidth: 2,
    borderRadius: 10,
    padding: 10,
    minWidth: '20%',
  },
  liste: {
    alignItems: "center",
    paddingBottom: 15,
    flexDirection: 'column'
  },
  recherche: {
    paddingTop: 150,
    alignContent: 'center',
    paddingLeft: 80,
    paddingBottom: 15,
    flexDirection: "row",
  },
  samelesbrise: {
    alignItems: "center"
  },
  button: {
    padding: 10,
    width: 80,
    backgroundColor: 'orange',
    marginVertical:10,
    borderRadius: 10,
    borderWidth: 2,
    alignItems: "center",
    alignContent: "center"
  },
  button2: {
    padding: 10,
    margin: 10,
    width: 100,
    backgroundColor: 'orange',
    marginVertical:10,
    borderRadius: 10,
    borderWidth: 2,
    padding: 10,
    alignItems: "center",
    alignContent: "center"
  },
  carte: {
    flexDirection: "column",
    paddingLeft: 5,
  },
  carte2: {
    flexDirection: 'row',
    borderWidth: 2,
    width: 400,
    backgroundColor: "beige",
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    marginBottom: 5
  },
  textcarte: {
    flexDirection: 'column',
    width: 250,
    justifyContent: "center",
    padding: 10
  },
  text2: {
    paddingBottom: 10,
    paddingTop: 10
  },  
  tinyLogo: {
    width: 106,
    height: 200,
    marginLeft: 10,
    marginRight: 20
  },


});
